import os
from flask import Flask, render_template, url_for, request, redirect

BASEDIR = os.path.abspath(os.path.dirname(__file__))
class Config (object):
    DEBUG=True
    TESTING=False
    CSRF_ENABLED=True
    SEND_FILE_MAX_AGE_DEFAULT=0
    SECRET_KEY='ChangeThis!'

class ProdConfig(Config):
    print 'Settings'
