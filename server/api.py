#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from flask import Flask, request, redirect, url_for
import requests
import base64
import json
import re
import os
import sys
app = Flask(__name__)
app.config.from_object('settings.ProdConfig')

@app.route('/')
def index():
    return 'z5112661 - UNSW zID API'

def unsw_login(username, password):
    # Let's define the URLs to use for the UNSW zID Single Sign-On
    login_url = "https://ssologin.unsw.edu.au/cas/login?service=https%3A%2F%2Fmy.unsw.edu.au%2Fportal%2FadfAuthentication"
    logout_url = "https://my.unsw.edu.au/portal/logout?end_url=https://ssologin.unsw.edu.au/cas/logout?service=https://my.unsw.edu.au/portal/adfAuthentication"
    logout_test = requests.post(logout_url)

    # Next, we will download the login page to extract the CSRF value
    test_auth1 = requests.get(login_url)

    print('[+] Attempting to fetch CSRF token', file=sys.stdout)

    # We are using some regex here to extract the hidden CSRF value off the page
    find_lt_blob = "name=\"lt\" value=\".*\" />"

    # Here, we find the CSRF value and append it, so that we can use it in our login request
    annoying_lt = ((re.search(find_lt_blob, test_auth1.text)).group()[17:-4]).replace(" ", "+")

    print(str(annoying_lt), file=sys.stdout)

    # Now we construct a request to emulate a browser login session
    login_headers = {
        "Accept-Language": "en-US,en;q=0.5",
        "Accept-Encoding": "gzip, deflate",
        "Upgrade-Insecure-Requests": "1",
        "Cookie": test_auth1.headers["Set-Cookie"][:43],
        "Content-Type": "application/x-www-form-urlencoded",
        "Referer": "https://ssologin.unsw.edu.au/cas/login?service=https%3A%2F%2Fmy.unsw.edu.au%2Fportal%2FadfAuthentication"
    }

    # We insert the provided details with the fetched regex value
    login_payload = "lt=%s&_eventId=submit&username=%s&password=%s&submit=Agree+and+sign+on" % (annoying_lt, username, password)

    # Let's try login to UNSW!
    test_auth2 = requests.post(login_url, headers=login_headers, data=login_payload)

    print('[+] Attempting to login with provided details', file=sys.stdout)

    # Now let us discard of the password and payload, since we don't want to be storing that data longer than necessary
    password = ''
    login_payload = ''

    # If the response contains a certain header, it indicates we have logged in successfully! Else, let's display an error
    if "X-ORACLE-DMS-ECID" in test_auth2.headers:
        print('[+] Successfully logged in!', file=sys.stdout)
        return True
    print('[+] Unable to find ORACLE success header', file=sys.stdout)
    return False

@app.route('/login', methods=["GET", "POST"])
def login():
    # Ensure the API request is a POST method
    if request.method == "POST":
        # Check that the header is posting JSON blob
        if request.headers['Content-Type'] == 'application/json':
            # Deconstruct the JSON post to get key, zid and pass
            request_data = request.json
            key = request_data['key']
            # Ensure the key is INFS3634, so not just anyone can abuse the API
            if not key == 'INFS3634':
                return 'Incorrect API key'
            
            # Some basic data validation
            zid = request_data['zid']
            if not len(zid) == 8 or not zid.startswith("z"):
                return 'Incorrect zID'
            try:
                # Check whether the login is correct by decoding password and returning true/false
                password = base64.b64decode(request_data['password'])
                result = unsw_login(zid, password)
                print(result, file=sys.stdout)
                if result == True:
                    return zid
                else:
                    return 'unauthenticated'
            except:
                return 'False'
        else:
            return 'Error: Incorrect use of API'
    return "Error: Incorrect use of API"
        
if __name__ == "__main__":
    app.run(host='0.0.0.0')
