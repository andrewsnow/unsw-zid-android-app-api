# README #

This README outlines prerequisites, installation and usage of the UNSW zID Android API.

Watch the demo on YouTube: https://youtu.be/QtoKkk8oKaU

## Server Setup ##
This API uses Python for the backend, Flask for web deployment, and Requests to handle the actual zID authentication.

### Server Requirements ###
1. Ensure Python 2.7.14 is installed from (https://www.python.org/downloads/)
2. Ensure Flask is installed from (http://flask.pocoo.org/docs/latest/installation/)
3. Install Requests for Python from (http://docs.python-requests.org/en/master/user/install/)

### Server Usage ###
Simply start the API server by commandline:
~~~~
python api.py
~~~~
Flask should handle the rest. :)

Now that the server is running, you should be able to visit it in your browser by navigating to:
~~~~
http://localhost:5000/
~~~~

You should get the response if installed correctly.:
~~~~
z5112661 - UNSW zID API
~~~~

The server is now running and can be used with your android app.

## Client Setup ##
Ensure the server is up and running before accessing the API through the client.

The only requirement for the client is Android Async HTTP Client.

### Client Requirements ###
1. Install and add the Android Async HTTP Client library from (http://loopj.com/android-async-http/)
2. Add 'client/UNSWAPI.java' to your Android project

### Client Usage ###
After you have installed the prerequisites, initialise the API in your activity that you wish to access, e.g. the Login Activity.
~~~~
UNSWAPI zLogin = new zLogin("url", "INFS3634", getApplicationContext());
~~~~
Replace URL with the URL of your api with /login appended to the end.

NOTE: if you are running the server locally, be sure to use your internal IP address for the URL as Android emulators classify localhost as the emulated device.

Next, create a static function to handle the login process with the following parameters
~~~~
public static void handleLogin(boolean authenticated) {
    // When authenticated is set to true, the user has been logged into UNSW.
}
~~~~

To perform a login attempt, simply use setters to set the zID and zPass of the API.
~~~~
zLogin.setzID("zID");
zLogin.setzPass("zPass");
~~~~

After the zID and zPass have been set, call the tryLogin() function.
~~~~
zLogin.tryLogin();
~~~~

(OPTIONAL) Basic zID validation is included as a boolean, which will return true if the zID is eight characters long and starts with z
~~~~
zLogin.validateLogin()
~~~~

### CONTACT ME ###
Andrew Snow [z5112661@ad.unsw.edu.au](mailto:z5112661@ad.unsw.edu.au)