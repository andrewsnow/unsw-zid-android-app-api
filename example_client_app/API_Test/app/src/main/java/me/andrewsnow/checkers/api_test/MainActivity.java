package me.andrewsnow.checkers.api_test;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    private EditText mUsernameText;
    private EditText mPasswordText;
    private Button mLoginButton;
    private UNSWAPI zLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mUsernameText = (EditText) findViewById(R.id.username);
        mPasswordText = (EditText) findViewById(R.id.password);
        mLoginButton = (Button) findViewById(R.id.login_button);
        zLogin = new UNSWAPI("http://192.168.0.23:5000/login", "INFS3634", getApplicationContext());
    }


    public void login(View view) {
        zLogin.setzID(mUsernameText.getText().toString());
        zLogin.setzPass(mPasswordText.getText().toString());
        try {
            zLogin.tryLogin();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void handleLogin(boolean authenticated) {
        if (authenticated) {
            System.out.println("SUCCESSFULLY LOGGED IN WITH " + UNSWAPI.responseResult);
        } else {
            System.out.println("Incorrect password");
        }
    }
}
